// Copyright (c) 2019-2021 James Gardner

package net.j_servers.james.demo_java_booth;


import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.VBox;
import javafx.scene.Scene;
import javafx.stage.Stage;


public class App extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(App.class.getResource("/intro.fxml"));
        VBox vbox = loader.load();

        Scene scene = new Scene(vbox);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Demo - Java Booth");
        primaryStage.show();

    }


    public static void main(String[] args) {
        launch(args);
    }

}

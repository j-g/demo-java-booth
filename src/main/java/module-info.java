// Copyright (c) 2019-2021 James Gardner

module net.j_servers.james.demo_java_booth {

    requires javafx.controls;
    requires javafx.fxml;
    requires org.slf4j;

    exports net.j_servers.james.demo_java_booth to javafx.graphics;

}

*Copyright (c) 2019-2021 James Gardner*

# Demo - Java Booth

## What It Is

A self-contained Java runnable, created solely to demonstrate usage of various
Java libraries, frameworks and services.

## How To Download It

There are separate packages for each of the major desktop platforms - Windows, OSX and Linux (all 64-bit).
Simply pick the file labeled for your operating system, from **[here](https://gitlab.com/j-g/demo-java-booth/-/jobs/artifacts/master/browse?job=create_artifact_build_runtimes)**.

## How To Run It

The demo is contained in a ZIP file. So:

1. Unzip the file you downloaded.
   (All the files inside the ZIP archive are contained under a parent folder, so
   you can extract it to your desktop without fear of littering it with multiple
   files or folders!)
2. In the resulting unzipped folder, go into the subfolder `bin`, and run/click the file:
   - `LaunchDemoJavaBooth.bat` (on Windows)
   - `LaunchDemoJavaBooth` (on OSX and Linux)

## If You Experience A Problem Or See A Bug

Create an issue [here](https://gitlab.com/j-g/demo-java-booth/issues).

## Disclaimer

While the demo software should not cause any problems on your system, or make
changes to it beyond those needed for running the demo (eg. possible creation of
temporary files), please note that permission to use the software is granted to
you only upon your agreement with the terms stated in the file named `LICENSE`
which accompanies the software, and in particular your attention is drawn to the
following paragraph within:

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
